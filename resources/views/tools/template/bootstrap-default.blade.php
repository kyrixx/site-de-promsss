
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="../../favicon.ico">

    <title>Starter Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('css/custom-styles.css') }}" rel="stylesheet">
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Cl214</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li {!! stripos(Route::currentRouteName(), 'news') !== False ? ' class="active"' : '' !!} ><a href="{{ url(route('news.index')) }}">Acceuil</a></li>
            <li {!! stripos(Route::currentRouteName(), 'links') !== False ? ' class="active"' : '' !!}><a href="{{ url(route('links.index')) }}">Liens</a></li>
            <li {!! stripos(Route::currentRouteName(), 'maps') !== False ? ' class="active"' : '' !!}><a href="{{ url(route('maps.index')) }}">Map de Prom'sss</a></li>
            <li><a href="#about">Offre de stage</a></li>
            <li><a href="#contact">Annuaire de Prom'sss</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <a href="#" class="btn btn-default navbar-btn">Déconnexion</a>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container-fluid primary">
        <div class="col-md-2">

        </div>
        <div class="col-md-10">
            @yield('content')
        </div>

    </div><!-- /.container -->

    @section('scripts')
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        $(function(){
            $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                    });
        });
    </script>
    @show
  </body>
</html>
