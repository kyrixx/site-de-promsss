@extends('tools.template.bootstrap-default')

@section('content')

<div class="row">
    <div class="col-md-8">
        <h1>Edition d'un article</h1>
        {{ Form::open(array('url' => route('news.update', $article), 'method' => 'PUT')) }}
           <div class="form-group">
                {{ Form::label('title', 'Titre') }}
                {{ Form::text('title', $article->title, array('class' => 'form-control')) }}
           </div>

           <div class="form-group">
                {{ Form::label('category_id', 'Catégorie') }}
                {{ Form::select('category_id', $categories, $article->category_id, array('class' => 'form-control')) }}
           </div>
           <div class="form-group">
                {{ Form::label('content', 'Contenu de l\'article') }}
                {{ Form::textarea('content', $article->content, array('class' => 'form-control')) }}
           </div>
           <button class="btn btn-primary">Soumettre l'article</button>
        {{ Form::close() }}
    </div>
</div>

@endsection