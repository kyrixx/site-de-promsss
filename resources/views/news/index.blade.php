@extends('tools.template.bootstrap-default')

@section('content')

<div class="row">
    <div class="col-md-12">

    <h1>Les dernières News !</h1>
    <a class="btn btn-primary" href="{{ route('news.create') }}">Ajouter un article</a>
    @foreach($news as $article)
        <div class="row panel panel-default news">
                        <div class="panel-heading"><h1>{{ $article->title }}</h1></div>
            <div class="panel-body">
                <div class="row">
                    <p><a href="{{ route('news.show', $article) }}">#{{ $article->id }}</a> Catégorie : {{ $article->category->name }}</p>
                    <a class="btn btn-primary" href="{{ route('news.edit', $article) }}">Editer</a>
                </div>
                <p>{{ $article->content }}</p>
                <p class="text-right"><em>Par {{ $article->profile->fullName() }}</em></p>
                <p>{{ $article->nbComments() }} commentaires</p>
            </div>
        </div>
    @endforeach

    </div>
</div>

@endsection