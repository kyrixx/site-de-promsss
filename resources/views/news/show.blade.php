@extends('tools.template.bootstrap-default')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row panel panel-default news">
            <div class="panel-heading"><h1>{{ $article->title }}</h1></div>
            <h5><a href="{{ route('news.show', $article->id) }}">#{{ $article->id }}</a> Catégorie : {{ $article->category->name }}</h5>
            <p>{{ $article->content }}</p>
            <p class="text-right"><em>Par {{ $article->profile->fullName() }}</em></p>
        </div>
        <div class="row">
            <h1>Laisser un commentaire</h1>
            {{ Form::open(array('method' => 'post', 'url' => route('news.storeComment', $article))) }}
                <div class="form-group">
                    {{ Form::label('comment', 'Commentaire') }}
                </div>
                <div class="form-group">
                    {{ Form::textarea('comment', null, ['class' => 'form-control', 'placeholder' => 'Laissez un commentaire ici']) }}
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Envoyer</button>
                </div>

            {{ Form::close() }}
        </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @foreach($article->comments as $comment)
                    <div class="row panel panel-default">
                        <div class="panel-heading">
                            <strong>{{ $comment->profile->fullName() }}</strong>
                        </div>
                        <p>{{ $comment->comment }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection