@extends('tools.template.bootstrap-default')

@section('content')
    <div class="row">
        <div class="col-md-10">
            <h1>Mais ils sont où mes Prom'sssquets ??</h1>

                <div class="form-group">
                    <div class="col-md-5">
                        <input id="pac-input-button" class="form-control" type="button" value="Enregistrer ma nouvelle position">
                    </div>
                    <div class="col-md-5">
                        <input id="pac-input-text" class="form-control" type="text" placeholder="Recherche ma position">
                    </div>
                </div>

         </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <div id="maps" style="height: 600px; width: 100%"></div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script src="{{ URL::asset('js/googlemap.js') }}"></script>
@endsection