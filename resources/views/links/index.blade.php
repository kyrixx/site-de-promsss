@extends('tools.template.bootstrap-default')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <a href="{{ route('links.create') }}" class="btn btn-primary">Créer un nouveau lien</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <table class="table table-condensed">
                <tr>
                    <th>#</th>
                    <th>Proposé par</th>
                    <th>URL</th>
                    <th>Description</th>
                    <th>Options</th>
                </tr>
                @foreach($links as $link)
                    <tr>
                        <td>{{ $link->id }}</td>
                        <td>{{ $link->profile->fullName() }}</td>
                        <td><a href="{{ $link->link }}">{{ $link->link }}</a></td>
                        <td>{{ $link->description }}</td>
                        <td>
                            <div class="form-group">
                                <a href="{{ route('links.edit', $link) }}" class="btn btn-primary">Editer</a>
                                {{ Form::open(array('url' => route('links.destroy', $link), 'method' => 'Delete')) }}
                                    <button class="btn btn-danger">Supprimer</button>
                                {{ Form::close() }}
                                <button href="" class="btn btn-warning">Signaler</button>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection