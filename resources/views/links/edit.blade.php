@extends('tools.template.bootstrap-default')

@section('content')
    <div class="row">
        <div class="col-md-10">
            {{ Form::open(array('url' => route('links.update', $link), 'method' => 'put')) }}
                <div class="form-group">
                    {{ Form::label('link', 'URL') }}
                    {{ Form::text('link', $link->link, ['class' => 'form-control', 'placeholder' => 'Entrez votre lien']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('description', 'Description') }}
                    {{ Form::text('description', $link->description, ['class' => 'form-control', 'placeholder' => 'Entrez une description claire']) }}
                </div>

                <div class="form-group">
                    <button class="btn btn-primary">Editez votre lien</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>

@endsection