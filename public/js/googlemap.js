var autocomplete;
var place;

function initMap() {
    geocoder = new google.maps.Geocoder();

    // On créé la carte
    var map = new google.maps.Map(document.getElementById("maps"), {
        zoom: 5,
        mapTypeControl: false,
        center: new google.maps.LatLng(48.85661400000001,2.3522219000000177) // Paris
    });

    // On restreint la recherche
    var autocomplete_options = {
        bounds: new google.maps.LatLngBounds(
            new google.maps.LatLng(43, -5),
            new google.maps.LatLng(51, 7)
        )
    };
    var input_text = document.getElementById("pac-input-text");
    autocomplete = new google.maps.places.Autocomplete(input_text, autocomplete_options);
    autocomplete.addListener('place_changed', function() {
        place = this.getPlace();
    });

    // On gère la mise à jour de la position
    var input_button = document.getElementById("pac-input-button");
    input_button.addEventListener('click', maj_localisation, false);
    function maj_localisation()
    {
        console.log(place.geometry.location.toJSON());
        $.post('maps/UserLocation',
            {
                localisation: place.geometry.location.toJSON()
            },
            null,
            'json'
        );
    }

    var markers = [];
    var infoWindow = new google.maps.InfoWindow({
        content: 'Info Window'
    });
    $.get('maps/UsersLocation',
        false,
        fillMap,
        'json');
    function fillMap(data)
    {
        data.forEach(function(value){

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(value),
                map: map
            });
            marker.addListener('click', function(){
                infoWindow.open(map, marker);
            });
            markers.push(marker);
        });
    }

    var france = new google.maps.LatLng(48.85661400000001,2.3522219000000177);

}

window.onload = function() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDqkkmNFWrSomSjLzPe9HwDAU2_eZpgUJU&libraries=places&callback=initMap';
    document.body.appendChild(script);
}
