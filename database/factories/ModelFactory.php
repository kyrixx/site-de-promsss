<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\News::class, function(\Faker\Generator $faker) {
    return [
        'profile_id' => $faker->numberBetween(1,100),
        'title' => $faker->sentence(4),
        'content' => $faker->paragraph(5),
        'category_id' => $faker->numberBetween(1,10)
    ];
});

$factory->define(App\NewsCategory::class, function(\Faker\Generator $faker) {
    return [
        'name' => $faker->word
    ];
});

$factory->define(App\NewsComment::class, function(\Faker\Generator $faker) {
    return [
        'news_id' => $faker->numberBetween(1,300),
        'comment' => $faker->paragraph(4),
        'profile_id' => $faker->numberBetween(1, 100)
    ];
});

$factory->define(App\UserProfile::class, function(\Faker\Generator $faker) {
    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'gender' => $faker->randomLetter,
        'mail' => $faker->email,
        'phone' => $faker->phoneNumber,
        'zoo' => '',
        'birthday' => $faker->date('Y-m-d'),
        'is_visible' => 1,
        'photo' => '',
        'buque' => $faker->word,
        'fams' => $faker->numberBetween(1,188),
        'mail_gadz' => $faker->companyEmail,
        'bande' => $faker->word,
        'boquette' => $faker->sentence(3),
        'bouls' => $faker->sentence(6),
        'created_at' => $faker->date('Y-m-d'),
        'updated_at' => $faker->date('Y-m-d')
    ];
});
