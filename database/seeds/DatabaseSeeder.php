<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Eloquent::unguard();
         // $this->call(UserProfileTableSeeder::class);
         factory(App\UserProfile::class, 100)->create();
         factory(App\NewsCategory::class, 10)->create();
         factory(App\News::class, 300)->create();
         factory(App\NewsComment::class, 1000)->create();
    }
}
