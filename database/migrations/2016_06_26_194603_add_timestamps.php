<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimestamps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_profiles', function(Blueprint $table) {
           $table->timestamps();
        });
        Schema::table('news_categories', function(Blueprint $table) {
           $table->timestamps();
        });
        Schema::table('links', function(Blueprint $table) {
           $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_profiles', function(Blueprint $table) {
            $table->dropTimestamps();
        });
        Schema::table('news_categories', function(Blueprint $table) {
            $table->dropTimestamps();
        });
        Schema::table('links', function(Blueprint $table) {
            $table->dropTimestamps();
        });

    }
}
