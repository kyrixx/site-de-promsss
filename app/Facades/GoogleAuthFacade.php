<?php
/**
 * Created by PhpStorm.
 * User: kapou_000
 * Date: 02/07/2016
 * Time: 18:48
 */

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class GoogleAuthFacade extends Facade {

    protected static function getFacadeAccessor() { return 'App\Librairies\GoogleAuth'; }

} 