<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = array('link', 'description', 'profile_id');

    public function profile()
    {
        return $this->belongsTo('App\UserProfile');
    }
}
