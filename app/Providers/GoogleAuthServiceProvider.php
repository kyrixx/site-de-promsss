<?php
/**
 * Created by PhpStorm.
 * User: Kapoujyan Stéphane
 * Date: 02/07/2016
 * Time: 18:26
 */

namespace App\Providers;


use App\Librairies\GoogleAuth;
use Illuminate\Support\ServiceProvider;

class GoogleAuthServiceProvider extends ServiceProvider {

    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Librairies\GoogleAuth', function(){
            return new GoogleAuth(new \Google_Client());
        });
    }

    public function provides()
    {
        return ['App\Librairies\GoogleAuth'];
    }
}