<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsComment extends Model
{
    protected $fillable = array('news_id', 'profile_id', 'comment');

    public function profile()
    {
        return $this->belongsTo('App\UserProfile');
    }

    public function news()
    {
        return $this->belongsTo('App\News');
    }
}
