<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    public function fullName()
    {
        return $this->buque . ' ' . $this->fams . ' dit ' . $this->lastname . ' ' . $this->firstname;
    }

    public function user()
    {
        return $this->hasOne('App\User');
    }


}
