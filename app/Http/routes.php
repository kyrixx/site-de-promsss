<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(array('middleware'=>'auth'), function()
{
    Route::get('/', function () {
        return redirect(url('/news'));
    });

    /*
    +--------+-----------+------------------+--------------+---------------------------------------------+------------+
    | Domain | Method    | URI              | Name         | Action                                      | Middleware |
    +--------+-----------+------------------+--------------+---------------------------------------------+------------+
    |        | GET|HEAD  | /                |              | Closure                                     | web        |
    |        | POST      | news             | news.store   | App\Http\Controllers\NewsController@store   | web        |
    |        | GET|HEAD  | news             | news.index   | App\Http\Controllers\NewsController@index   | web        |
    |        | GET|HEAD  | news/create      | news.create  | App\Http\Controllers\NewsController@create  | web        |
    |        | DELETE    | news/{news}      | news.destroy | App\Http\Controllers\NewsController@destroy | web        |
    |        | PUT|PATCH | news/{news}      | news.update  | App\Http\Controllers\NewsController@update  | web        |
    |        | GET|HEAD  | news/{news}      | news.show    | App\Http\Controllers\NewsController@show    | web        |
    |        | GET|HEAD  | news/{news}/edit | news.edit    | App\Http\Controllers\NewsController@edit    | web        |
    +--------+-----------+------------------+--------------+---------------------------------------------+------------+
    */
    Route::resource('news', 'NewsController');
    Route::post('news/comment/{news}', ['as' => 'news.storeComment', 'uses' => 'NewsController@storeComment']);


    Route::resource('links', 'LinksController');
    Route::get('/maps/UserLocation/', ['as' => 'maps.getUserLocation', 'uses' => 'MapsController@getUserLocation']);
    Route::post('/maps/UserLocation/', ['as' => 'maps.postUserLocation', 'uses' =>'MapsController@postUserLocation']);
    Route::get('/maps/UsersLocation/', ['as' => 'maps.getLocations', 'uses' => 'MapsController@getLocations']);
    Route::resource('maps', 'MapsController');
});

Route::auth();