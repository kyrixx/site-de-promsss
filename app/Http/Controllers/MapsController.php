<?php

namespace App\Http\Controllers;

use App\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MapsController extends Controller
{
    public function __construct()
    {
        $this->middleware('ajax', ['except' => ['index', 'getLocations']]);
    }

    public function index()
    {
        return view('maps.index');
    }

    public function getUserLocation()
    {
        $profile = UserProfile::findOrFail(Auth::user()->user_profile_id);
        return response()->json($profile->localisation);
    }

    public function postUserLocation(Request $request)
    {
        $profile = UserProfile::findOrFail(Auth::user()->user_profile_id);
        $profile->localisation = $request->get('localisation');
        $profile->save();
    }

    public function getLocations()
    {
        $localisations = UserProfile::lists('localisation');
        return response()->json($localisations->all());
    }
}
