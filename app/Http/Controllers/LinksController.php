<?php

namespace App\Http\Controllers;

use App\Link;
use Illuminate\Http\Request;

use App\Http\Requests;

class LinksController extends Controller
{
    public function index()
    {
        $links = Link::all();
        return view('links.index', compact('links'));
    }

    public function create()
    {
        return view('links.create');
    }

    public function store(Request $request)
    {
        if(stripos($request->get('link'), 'http://') == FALSE AND stripos($request->get('link'), 'https://') == FALSE)
            $request->merge(array('link' => 'http://'.$request->get('link')));
        $request->merge(array('profile_id' => 1));
        Link::create($request->except('_token'));
        return redirect(route('links.index'));
    }

    public function edit($id)
    {
        $link = Link::findOrFail($id);
        return view('links.edit', compact('link'));
    }

    public function update($id, Request $request)
    {
        if(stripos($request->get('link'), 'http://') == FALSE AND stripos($request->get('link'), 'https://') == FALSE)
            $request->merge(array('link' => 'http://'.$request->get('link')));
        Link::find($id)->update($request->except('_token'));
        return redirect(route('links.index'));
    }

    public function destroy($id)
    {
        Link::destroy($id);
        return redirect(route('links.index'));
    }
}
