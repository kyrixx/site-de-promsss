<?php

namespace App\Http\Controllers;

use App\NewsCategory;
use App\NewsComment;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\News;

class NewsController extends Controller
{
    public function index()
    {
    	$news = News::take(10)->orderBy('id', 'DESC')->get();
    	return view('news.index', compact('news'));
    }

    public function create()
    {
    	$categories = NewsCategory::lists('name', 'id');
        return view('news.create', compact('categories'));
    }

    public function store(Request $request)
    {
    	$profile_id = 1;
        $request->merge(['profile_id'=>$profile_id]);
        News::create($request->except('_token'));
        return redirect(route('news.index'));
    }

    public function storeComment($id, Request $request)
    {
    	$profile_id = 1;
        $news_id = $id;
        $request->merge(compact('profile_id', 'news_id'));
        NewsComment::create($request->except('_token'));
        return redirect(route('news.show', $id));
    }

    public function show($id)
    {
        $article = News::findOrFail($id);
    	return view('news.show', compact('article'));
    }

    public function edit($id)
    {
    	$article = News::findOrFail($id);
        $categories = NewsCategory::lists('name', 'id');
    	return view('news.edit', compact('article', 'categories'));
    }

    public function update($id, Request $request)
    {
    	$article = News::findOrFail($id);
        $article->update($request->except('_token'));
        return redirect(route('news.show', $id));
    }

    public function destroy($id)
    {
    	News::destroy($id);
    }
}
