<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = ['title', 'content', 'profile_id', 'category_id'];

    public function nbComments()
    {
        return $this->comments->count();
    }

    public function category()
    {
    	return $this->belongsTo('App\NewsCategory');
    }

    public function comments()
    {
    	return $this->hasMany('App\NewsComment');
    }

    public function profile()
    {
        return $this->belongsTo('App\UserProfile');
    }
}
